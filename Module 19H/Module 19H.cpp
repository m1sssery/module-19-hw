﻿#include <iostream>

using namespace std;

class Animal
{
public:

    virtual void voice() const = 0;

};

class Cat : public Animal
{
public:
    void voice() const override
    {
        cout << "Meow!\n";
    }

};

class Dog : public Animal
{
    void voice() const override
    {
        cout << "Woof!\n";
    }
};

class Cow : public Animal
{
    void voice() const override
    {
        cout << "tylko jedno w glowie mam\n";
    }
};

int main()
{
    Animal* animals[3];
    animals[0] = new Cat();
    animals[1] = new Dog();
    animals[2] = new Cow();

    for (Animal* a : animals)
        a->voice();
}

/* если вы это прочитали, напишите в сообщении - :) */
